<?php
if(!isset($_SESSION['id'])){
	session_start();
	$_SESSION['id'] = session_id();
}
if(isset($_POST['type'])){
	$_SESSION['type'] = $_POST['type'];
}
if(isset($_POST['username'])){
	$_SESSION['username'] = strtolower($_POST['username']);
}
if(isset($_POST['action'])){
	$_SESSION['action'] = $_POST['action'];
}
if(isset($_POST['origin'])){
	$_SESSION['origin'] = $_POST['origin'];
}
if(isset($_POST['template'])){
	$_SESSION['template'] = $_POST['template'];
}
// For templates, this is a file but for slides, this is a directory
if(isset($_POST['file'])){
	$_SESSION['file'] = $_POST['file'];
}
if(isset($_POST['format'])){
	$_SESSION['format'] = $_POST['format'];
}
?>