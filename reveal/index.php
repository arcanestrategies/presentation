<?php
session_start();
$_SESSION['id'] = session_id();
//require_once('constants.php');
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>Rumble Presentation Editor</title>
		
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/black.css">
		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="lib/css/zenburn.css">
		<!-- Custom Rumble Theme -->
		<link rel="stylesheet" href="css/rumble.css">
		<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script type="text/javascript">
			var dialog = function (){
				$('#quick').show();
			}
			$(document).ready(function(){
				if($('body').data('user').length == 0){
					$('#dialog.lightbox').show();
					$('#username').show();
				}
				$('#username input[type="submit"]').click(function(e){
					e.preventDefault();
					var username = $('#username input[name="uname"]').val();
					username = username.toLowerCase();
					$.post("session.php", {'username':username}, function(data){
						$('.lightbox').hide();
						$('#username').hide();
						$('body').data('user',username);
					});
				});
				$('.closer').click(function(){
					$('#quick').hide();	
				});
				$('#quickform').submit(function(e){
					e.preventDefault();
					var quicktemp = function(){
						$.post('/includes/quick.php', data, function(response){
							// Doing nothing
						});
					}
					quicktemp();
				});
				$("#navigation .btn").click(function(){
					//$("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
					$(this).attr("clicked", "true");
					data = {};
					if($(this).attr('data-action')){
						var action = $(this).data('action');
						var type = $(this).data('type');
						data.action = action;
					}
					if($(this).attr('data-template')){
						var template = $(this).data('template');
						if(template=='quick'){
							dialog();
							return;
						}
						data.template = template;
					}
					if($(this).attr('data-file')){
						var file = $(this).data('file');
						data.file = file;
					}
					if($(this).attr('data-type')){
						var type = $(this).data('type');
						if(type == 'quick'){
							data.postall = $('#quickform').serialize();
							data.postdata = $('#quickform input[name="quick"]').val();
							//data.file = $('#quickform input[name="filename"]').val();
						} else {
							data.origin = type;
						}
						data.type = type;
					} else {
						var type = false;
					}
					var sessioner = function(){
						$.post("session.php", data, function(ret){
							if(typeof data['template'] !== 'undefined'){
								window.location.href = '/editor';
								$('#menu-top').hide();
							}
							console.log(ret);
						});
					}
					if(typeof data.type !== 'undefined' && data.type == 'quick'){
						$('#quickform button').submit();
					};
					sessioner();
					$('.sub-menu').hide();
					$('#menu-'+action).show();
					$('.submenutype').hide();
					$('#submenu-'+type).show();
				});
				$("#menu-edit .fa-file").click(function(){
					if($(this).attr('data-type')){
						var type = $(this).data('type');
						data.type = type;
					}
					//sessioner();
				});
				$(".fa-folder").click(function(){
					$('.filelist').hide();
					$(this).nextAll('.filelist:first').show();
				});
			});
		</script>
	</head>
	<body data-user="<?php if(isset($_SESSION['username'])){ echo $_SESSION['username']; } ?>">
		<?php require_once('includes/dialog.php'); ?>
		<!-- Menu for either "Create New" presentation or "Edit Existing" presentation -->
		<?php require_once('includes/header.php'); ?>
	</body>
</html>
