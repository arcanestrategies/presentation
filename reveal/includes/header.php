<?php
require_once('constants.php');
?>
<div id="navigation">
		<div id="quick" class="lightbox hidden">
				<div class="wrapper">
					<div class="closer"></div>
					<form id="quickform">
						<!--div><h2>Enter your new slide name</h2></div>
							<input type="text" name="filename" value=""-->
						<div><h2>Enter the punches to add to the template<span class="req"><sup>*</sup></span></h2></div>
						<div><span>(1 = Jab, 2 = Cross, 3 = Front Hook, 4 = Back Hook, 5 = Front Upper, 6 = Back Upper, D = Duck, F = Fire)</span></div>
						<div>
							<input type="text" name="quick" maxlength="7" value="">
							<button class="btn" class="btn" data-file="<?php echo TEMP; ?>" data-template="<?php echo session_id(); ?>" data-action="create" data-type="quick">Create Template</button>
						</div>
					</form>
				</div>
		</div>
		<div id="menu">
			<div id="menu-top">
				<?php
					foreach($categories as $type){
						foreach(array('create') as $action){
							$btn = '<a href="#" class="btn" data-action="'.$action.'" data-type="'.$type.'">'.ucfirst($type).' Presentation</a>';
							echo $btn;
						}
					}
				?>
				<a href="#" class="btn" data-action="edit">Edit Presentation</a>
			</div>
		</div>
		<div id="menu-create" class="hidden sub-menu">
			<h2>Select A Template</h2>
			<a href="#" class="btn" data-template="quick">Quick Template</a>
			<?php
				foreach($templates as $template){
					$temps = [];
					foreach(glob(TEMPLATES.$template.'/*', GLOB_ONLYDIR) as $dir) {
						$temps[] = basename($dir);
					}
					echo('<div id="submenu-'.$template.'" class="submenutype"><h3>'.ucfirst($template).'</h3>');
					/*$parts = scandir(TEMPLATES.$template.'/');
					array_shift($parts); array_shift($parts);*/
					foreach($temps as $temp){
						echo '<a href="#" class="fa fa-folder" aria-hidden="true" data-file="'.TEMPLATES.$template.'/" data-template="'.$temp.'">&nbsp;&nbsp;'.clean($temp).'</a>';
						$parts = scandir(TEMPLATES.$template.'/'.$temp);
						array_shift($parts); array_shift($parts);
						echo '<div class="filelist">';
						foreach($parts as $part){
							echo '<a href="#" class="btn fa fa-file mini" aria-hidden="true" data-file="'.TEMPLATES.$template.'/'.$temp.'/" data-template="'.$part.'">&nbsp;&nbsp;'.clean(basename($part,".html")).'</a>';
						}
						echo '</div>';
					}
					echo('</div>');
				}
			?>
		</div>
		<div id="menu-edit" class="hidden sub-menu">
			<h2>Select A Presentation</h2>
			<?php
				//$presentations = scandir (SLIDES.$_SESSION['type'].'/');
				foreach($categories as $type){
					$presentations = [];
					foreach(glob(SLIDES.$type.'/*', GLOB_ONLYDIR) as $dir) {
						$presentations[] = basename($dir);
					}
					echo('<h3>'.ucfirst($type).'</h3>');
					foreach($presentations as $show){
						echo '<a href="#" class="fa fa-folder" aria-hidden="true" data-type="'.$type.'" data-file="'.$show.'" data-template="'.$show.'">&nbsp;&nbsp;'.clean($show).'</a>';
						$parts = glob(SLIDES.$type.'/'.$show.'/*', GLOB_ONLYDIR);
						echo '<div class="filelist">';
						foreach($parts as $part){
							$filetime = date("F d Y g:i:s A",filemtime($part));
							echo '<a href="#" class="btn fa fa-file mini" aria-hidden="true" data-file="'.$part.'/" data-type="'.$type.'" data-template="'.$part.'/">&nbsp;&nbsp;<strong>'.basename($part).'</strong>&nbsp;&nbsp;<em>'.$filetime.'</em></a>';
						}
						echo '</div>';
					}
				}
			?>
		</div>
		<div id="save-options" class="hidden ">
			<a href="/" class="btn inline-block">< Return Home</a>
			<?php 	$act = ($_SESSION['action'] == 'edit')? 'slide':'template';
					$opp = ($_SESSION['action'] == 'edit')? 'template':'slide';
				$btn = '<a href="#" data-format="'.$opp.'" data-action="create" class="btn inline-block">Save As '.ucfirst($opp).'</a>';
				$btn .= '<a href="#" data-format="'.$act.'" data-action="create" class="btn inline-block">Save As '.ucfirst($act).'</a>';
				$btn .= '<a href="#" data-format="'.$act.'" data-action="edit" class="btn inline-block">Save Current '.ucfirst($act).'</a>';
				$btn .= '<a href="#" id="newpresentation" data-format="slide" data-action="create" class="btn inline-block">Create New Presentation</a>';
				echo($btn);
			?>
			<!--form method="post" class="inline-block">
					<div>
						<input id="slideCheck" type="checkbox" name="type" value="slide">
						<label for="slideCheck">Save As New Slide</label>
					</div>
					<div>
						<input id="editCheck" type="checkbox" name="type" value="edit">
						<label for="editCheck">Save Slide Edits</label>
					</div>
					<div>
						<input id="templateCheck" type="checkbox" name="type" value="template">
						<label for="templateCheck">Save As New Template</label>
					</div>
					<button type="button" class="saveas btn" value="">Save As</button>
			</form-->
		</div>
</div>
