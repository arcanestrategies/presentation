<?php
$gallery = [];
if(!defined('MEDIA')){
	require_once('constants.php');
}
foreach(glob(MEDIA.'*', GLOB_ONLYDIR) as $f) {
	$gallery[] = basename($f);
}
$html = '';
foreach($gallery as $category){
			$html .= '<div id="dir-'.$category.'">';
			$html .= '<h2>'.$category.'</h2>';
			$items = scandir(MEDIA.$category);
			array_shift($items); array_shift($items);
			foreach($items as $item){
				$html .= '<img src="images/'.$category.'/'.$item.'" width="30px;" height="auto;" />';
			}
			$html .= '</div>';
		}
echo $html;
?>