<?php
require('directorysettings.php');

define('HTTP','http://rumble-presentations.arcanestrategies.com/');
//define('ROOT','C:/xampp/rumble-deck/reveal/');
define('SLIDES',ROOT.'slides/');
define('TEMP',ROOT.'tmp/');
define('TEMPLATES',ROOT.'templates/');
define('MEDIA',ROOT.'images/');
define('HTTPMEDIA',HTTP.'images/');
define('JS',ROOT.'js/');
$templates = $categories = [];
foreach(glob(TEMPLATES.'*', GLOB_ONLYDIR) as $f) {
	$templates[] = basename($f);
}
foreach(glob(SLIDES.'*', GLOB_ONLYDIR) as $d) {
	$categories[] = basename($d);
}
function clean($filename){
	$filename = str_replace('_', ' ', $filename);
	return $filename;
}
?>