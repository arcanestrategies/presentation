<?php
if(!defined('MEDIA')){
	require_once('constants.php');
}
if (!empty($_POST)) {
	$id = session_id();
	$puncharray = ['1'=>HTTPMEDIA.'1.png','2'=>HTTPMEDIA.'2.png','3'=>HTTPMEDIA.'3.png','4'=>HTTPMEDIA.'4.png','5'=>HTTPMEDIA.'5.png','6'=>HTTPMEDIA.'6.png','d'=>HTTPMEDIA.'duck.gif','f'=>HTTPMEDIA.'fire.png'];
	if(isset($_POST['postdata'])&&!empty($_POST['postdata'])){
		/*$file = (!isset($_POST['file'])||empty($_POST['file']))? $id : preg_replace('/[^a-zA-Z0-9_]/', '', $_POST['file']);
		$file = TEMP.$file.'.html';*/
		$file = $_POST['file'].$_POST['template'].'.html';
		$newfile = fopen($file, "w");
		$punches = str_split($_POST['postdata']);
		$string = '<div class="editable">';
		foreach($punches as $punch){
			if($punch=='d'||$punch=='f'||is_numeric($punch)){
				list($width, $height, $type, $attr) = getimagesize ($puncharray[$punch]);
				$string .= '<span class="quick-img"><img width="'.$width.'" height="'.$height.'" src="'.$puncharray[$punch].'" /></span>';
			}
		}
		$string .= '</div>';
		fwrite($newfile, $string);
		fclose($newfile);
		echo 'filename '.$file.' created';
	} else {
		echo 'Whoops, you are missing information!';
	}
}

?>