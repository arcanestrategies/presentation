<?php
session_start();
$_SESSION['id'] = session_id();

require_once('includes/constants.php');

/*
	savehtml saves the tinymce data to the html file using the following process:
	(1) If the post data contains a filename, get the content from that filename and search for all instances of editable content, to overwrite.
	(2) If the post data does not contain a filename, we will skip the find and replace process and instead we'll write all editable tinymce data as divs.
*/
function edithtml($filepath,$postdata){
	$html = $current = get_html_from_file($filepath);
	$needle = 'editable';
	$offset = 0;
	// counts total number of "editable" instances in the file.
	$count = substr_count($current, $needle);
	$edits = [];
	$m = 0;

	foreach($postdata as $key=>$item){
		if(false !== strpos($key,'mce')){
			$edits[] = $item;
			if(strlen($item)<1){ $m++; }
		}
	}
	/*
	// if all mce's are empty, that's because they didn't ever touch the form and just clicked save, so we'll just return the original html;
	if($m == count($edits)){
		return $html;
	} else {*/
		// iterate through each of the counted editable instances
		for($i=0; $i<$count; $i++){
			// find the position of the current editable string
			$editable = strpos($html, $needle, $offset);
			// find the position of the closing bracket which follows, then return the start of the text to follow... essentially the editable field's content
			$offstart = strpos($html, '>', $editable)+1;
			// Now find the next </div> tag as that's likely the close of the editable tag.
			$offset = strpos($html, '</div>', $offstart);
			// Now get the length of chars betweeen offset and offstart
			$len = $offset - $offstart;
			// Not get the substring... don't think I actually need this
			$substr = substr($html,$offstart,$len);
			// now replace the contents of the string between offstart and offset with the nth posted value
			$html = substr_replace($html,$edits[$i],$offstart,$len);
		}
	//}
	
	return $html;
}

function savehtml($postdata){
		$action = isset($postdata['action'])? $postdata['action'] : 'edit';	// I think that the whole "Create" workflow was screwing with things
		$format = isset($postdata['format'])? $postdata['format'] : $_SESSION['format'];		// ie. "slide" or "template"
		$type = isset($postdata['type'])? preg_replace("![^a-z0-9]+!i", "_",$postdata['type']) : $_SESSION['type'];				// REFACTOR: In V1, there's no post data for type
		if(isset($_SESSION['origin'])&&$type=='quick'){
			$type = $_SESSION['origin'];
		}
		$username = isset($_SESSION['username'])? $_SESSION['username'] : 'misc';
		if(isset($postdata['filenew'])&&strlen($postdata['filenew'])>0&&$action=='create'){
			$filename = preg_replace("![^a-z0-9]+!i", "_",$postdata['filenew']);
			$original = $postdata['filename'];
		} else if(isset($postdata['filename'])){
			$original = $filename = $postdata['filename'];
		} else if(isset($_SESSION['file'])&&isset($_SESSION['template'])){
			$original = $_SESSION['file'].$_SESSION['template'];
			// I guess we don't care about filename here?  This is all kinda useless
		} else {
			$original = $filename = $_SESSION['filename'];
		}
		
		$dir = ($format == 'template')? TEMPLATES : SLIDES;
		
		if($action == 'create'){		
			$path = ($format == 'template')? $dir.$type.'/'.$username.'/' : $dir.$type.'/'.$username.'/'.$filename.'/';
			$filename = ($format == 'template')? $filename : uniqid();
			// In the case where we are storing a template file, we store it as HTML within the user's directory
			$filepath = $path.$filename.'.html';
			// If this user doesn't yet have a directory, we'll make it.
			if(!file_exists($path)){
				mkdir($path, 0755);
			}
			// If we're copying a template as a slide in a new location, we need to set the session variable as a 
			copy($original,$filepath);
			$_SESSION['file'] = $path;
			$_SESSION['type'] = $type;
			$_SESSION['action'] = ($action == 'create' && $format == 'slide')? $action : 'edit';
			$_SESSION['format'] = $format;
			$_SESSION['template'] = $filename.'.html';
		} else {
			$filepath = $original;
		}
		
		$html = edithtml($filepath,$postdata);
		
		if ( save_html_to_file($html, $filepath) ){
			header("Location: {$_SERVER['HTTP_REFERER']}");
		} else {
			echo ('Could not write to stream '. $filepath);
		}
		exit;
}

/**
 * 
 * @param string $content HTML content from TinyMCE editor
 * @param string $path File you want to write into
 * @return boolean TRUE on success
 */
function save_html_to_file($content, $path){
   return (bool) file_put_contents($path, $content);
   //chmod($path, 0664);
}

function get_html_from_file($path){
   return file_get_contents($path);
}
?>
