<?php
session_start();
$_SESSION['id'] = session_id();
header('Cache-Control: no-cache');
header('Pragma: no-cache');
//require_once('include/constants.php');
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta http-equiv="Expires" content="Tue, 01 Jan 1995 12:12:12 GMT">
		<meta http-equiv="Pragma" content="no-cache">
		<title>Rumble Presentation Editor</title>

		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/black.css">
		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="lib/css/zenburn.css">
		<!-- Custom Rumble Theme -->
		<link rel="stylesheet" href="css/dropzone.css">
		<link rel="stylesheet" href="css/rumble.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
			document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>
		<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=03grjm5to13kpgtd2qydv7hlk8ekkoj7271ebvk211ydobuy"></script>
		<script type="text/javascript">
			tinymce.init({
				selector: 'div.slides div.editable',
				paste_data_images: true,
				//images_upload_credentials: true
				inline: true,
				/*
					autosave=>restoredraft				// autosaves.  This seems important.
					charmap=>charmap=>insert			// adds a dialog to editor with map of special inicode chars if desired.  This seems optional.
					textcolor colorpicker=>forecolor backcolor	// color selections.  This seems important.
					emoticons=>emoticons=>emoticons		// adds emoticons.  This seems important for rumble.
					https://www.tinymce.com/docs/plugins/mediaembed/ // Premium plugin allows for video... not currently installed
					https://www.tinymce.com/docs/plugins/imagetools/ // Plugin for image editing... seems optional
					media=>insert=>media				// allows for addition of HTML5 video and audio
					https://www.tinymce.com/docs/plugins/moxiemanager/	// Paid premium file upload capability, must be loaded with "external_plugins" option... this seems necessary to use unless we create our own upload file capability and tie it in with https://www.tinymce.com/docs/plugins/image/ REFACTOR
					noneditable							// allows you to fix an HTML tag as non-editable by using "mceNonEditable" class :)
					preview=>view=>preview				// preview what you're currently editing... enabled but seems unnecessary given that we are doing in-line.
					save=>save							// seems redundant if we're doing auto-save but it's enabled.
					https://www.tinymce.com/docs/plugins/tinymcespellchecker/	// optional spell checker... not enabled.
					textcolor=> forecolor backcolor		// we already wave text color included as a result of color picker.
					visualblocks=>view=>visualblocks	// very cool capability to view visual blocks
					image->insert=>image				// the issue with this is that we have to define the image list in advance, so we might just have to do a thing where we do a basic HTML upload form and select from a list looped through?
				*/
				plugins: [
					'autosave save charmap textcolor colorpicker emoticons media noneditable preview code'
				],
				/*external_plugins: {
					'myplugin': '/js/myplugin/plugin.min.js'
				}*/
				// 1st toolbar is background?  2nd is each block?  Or just use the same everywhere?
				//toolbar: 'save newdocument restoredraft | undo redo removeformat | preview visualblocks | bold italic underline strikethrough subscript superscript | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | cut copy paste | bullist numlist | outdent indent | blockquote charmap media image | forecolor backcolor',
				toolbar1: 'save newdocument restoredraft | undo redo removeformat | bold italic underline strikethrough subscript superscript | alignleft aligncenter alignright alignjustify',
				toolbar2: 'styleselect formatselect fontselect fontsizeselect | bullist numlist | outdent indent | blockquote charmap media | forecolor backcolor',
				menubar: 'file edit format view insert',
				//menubar: false,
				/*menu: {
					view: {title: 'Rumble', items: ''}
				}*/
				//a_plugin_option: true,
				//a_configuration_option: 400
			});
			$(window).load(function() {
				$('.SubmitBtn').click(function(){
					//This MUST alert HTML content of editor.
					alert( tinyMCE.activeEditor.getElement() );
				});
			});
			$(document).ready(function(){
				function saver(action,format,filename,callback){
					$('.lightbox').hide();
					$('.lightbox > div').hide();
					$.post("session.php", {'format': format, 'action': action}, function(d){
						// Do nothing?
					});
					var data = {'format':format,'action':action,'filenew':filename};
					data['filename'] = $('#slideformer #filename').val();
					$('#slideformer .editable').each(function(){
						var mceid = $(this).attr('id');
						data[mceid] = $(this).html();
					});
					$.post("savethis.php", data, function(d){
						// show an indicator
						$("#save-options .btn").addClass('submitter').delay(5000).queue(function(next){
							$("#save-options .btn").removeClass('submitter');
							next();
						});
						console.log(d);
					});
					 if (typeof(callback) == 'function') {
						callback();
					 }
				}
				$('#menu-top').hide();
				$('#save-options').show();
				$("#menu_form").submit(function(e){
					e.preventDefault();
					var val = $("button[type=submit][clicked=true]").val();
					$.post("session.php", {'type': val});
				});
				var action = '';
				var format = '';
				var filename = '';
				var type = '';
				$('#filename .submit').click(function(e){
					e.preventDefault();
					filename = $('#filename input[name="fname"]').val();
					saver(action,format,filename, function(){
						window.location.reload();
					});
				});
				$('#presentation .submit').click(function(e){
					e.preventDefault();
					var form = $(this).parent();
					if(form.attr('id')=='new'){
						var field = form.find('input[name="pname"]');
						filename = field.val();	
					} else {
						var field = form.find('select[name="pname"]');
						filename = field.val();
					}
					console.log(action+' '+format+' '+filename);
					saver(action,format,filename, function(){
						window.location.reload();
					});
				});
				$("#save-options .btn").click(function(){
					//$("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
					$(this).attr("clicked", "true");
					action = $(this).data('action');	// create or edit
					format = $(this).data('format');	// presentation or template?
					//type = $(this).data('type');
					if(action=='create'){
						$('#dialog.lightbox').show();
						if(format=='slide'){
							$('#presentation').show();
							if($(this).attr('id')&&($(this).attr('id')=='newpresentation')){
								$('#presentation #existing').hide();
							}
						} else {
							$('#filename').show();
						}
					} else {
						filename = $('#filename').val();
						saver(action,format,filename);
					}
				});
				$("#preview > div.thumbnail-container > div.thumbnail").click(function(){
					var slide = $(this).attr("data-thumbnail");
					var flag = false;
					$('.slides section').each(function(){
						// The slides will load in order of DOM, so everything is in the "past" until it hits our clicked slide, then it's in the "future"
						if($(this).attr('id') == 'slide'+slide){
							flag = true;
							$(this).attr( "class", "present" );	/* replaces all slide classes with the present */
							//$(this).removeAttr( "hidden" );	/* replaces all slide classes with the present */
							$(this).prop('hidden',false);
							$(this).removeAttr( "aria-hidden" );	/* replaces all slide classes with the present */
						} else {
							if(flag == false){
								// if we're on previous slides, the next slide navigation should be enabled
								$('.reveal aside button.navigate-left').removeAttr('disabled');
								$('.reveal aside button.navigate-left').addClass('enabled');
							} else {
								$('.reveal aside button.navigate-right').removeAttr('disabled');
								$('.reveal aside button.navigate-right').addClass('enabled');								
							}
							$(this).attr("aria-hidden","true");
							$(this).prop('hidden',true);
							var position = (flag == true)? 'future' : 'past';
							$(this).attr( "class", position );
						}
					});
				});
				$(".tab").click(function(){
					var par = $(this).parent();
					var width = par.width();
					if(width == 80 ){
						par.animate({
							width: 600,
						}, 200 );
					} else {
						par.animate({
							width: 80,
						}, 200 );
					}
				});
			});
			$(document).ready(function(){
				var refreshmedia = function(){
						$.post('/includes/getfiles.php', {}, function(data){
							$('#media-container').html(data);
						});
				}
				$('.refresh').click(function(){
						refreshmedia();
				});
				var flag = true;
				var refresher = setInterval(function(){
						if($('.dz-complete').length && flag == true){
							refreshmedia();
							flag = false;
						}
				},1000);		
			});
			function allowDrop(ev) {
				ev.preventDefault();
			}
			function showSpots(ev) {
				var i = 0;
				ev.preventDefault();
				if(typeof ev.target != 'undefined'){
					if(typeof ev.target.nextSibling != 'undefined'){
						var id = ev.target.nextSibling.id;
						$(id).after('<div id="box'+i+'" class="thumbnail-container mid-zone hidden" ondrop="drop(event)" ondragover="allowDrop(event)"></div>');
					}
					if(typeof ev.target.previousSibling != 'undefined'){
						var id = ev.target.prevSibling.id;
						$(id).before('<div id="box'+i+'" class="thumbnail-container mid-zone hidden" ondrop="drop(event)" ondragover="allowDrop(event)"></div>');
					}
				}
			}
			function drag(ev) {
				ev.dataTransfer.setData("text", ev.target.id);
			}
			function drop(ev) {
				var finalized = false;
				ev.preventDefault();
				var data = ev.dataTransfer.getData("text");
				// if "data" is the id of the origin, then ev.target.id is the id of current
				var current = ev.target.parentElement.parentElement;
				var currentId = current.id;
				var currentData = document.getElementById(currentId).outerHTML;
				var newData = document.getElementById(data).outerHTML;
				document.getElementById(currentId).outerHTML = newData;
				document.getElementById(data).outerHTML = currentData;
				var order = [];
				var finalized = true;
				if(finalized == true){
					// put the order of each child of the boxes into an array, to pass the new order 
					$('.thumbnail-container .thumbnail').each(function(){
						var id = $(this).attr('data-thumbnail');
						order.push(id);
					});
					//ev.target.appendChild(document.getElementById(data));
					$.post("renamer.php", {'fileorder': order }, function(data){
							console.log(data);
							//window.location.reload();
					});
					// fire the ajax event
				}
			}		
		</script>
		<script src="js/dropzone.js"></script>
	</head>
	<body data-user="<?php echo $_SESSION['username']; ?>">
		<?php require_once('includes/dialog.php'); ?>
		<?php require_once('includes/sidebar.php'); ?>
		<?php require_once('includes/header.php'); ?>
		<?php require_once('includes/media.php'); ?>
		<div class="reveal">
			<div class="slides">
			<?php
			//	Get selected loaded slideshow
			if(null !== $_SESSION['action'] && null !== $_SESSION['file']){
				if($_SESSION['action'] == 'edit'){			// if we're editing, we assume we're editing slides
					$slides = scandir($_SESSION['file']);	// editing is done for a whole presentation, so we get the directory
					array_shift($slides);
					array_shift($slides);
					usort($slides, function ($a, $b){
						return substr($a, -7) - substr($b, -7);
					});
				} elseif($_SESSION['action'] == 'create' && $_SESSION['type'] == 'quick') {
					$slides = [$_SESSION['template'].'.html'];
				}	else {
					$slides = [$_SESSION['template']];		// templates are grabbed as a single file
				}
				foreach($slides as $slide){
					$html_open 	 =	'<section id="slide'.basename($slide,'.html').'"><form id="slideformer" method="post" action="savethis.php">';
					$html_open	.=	'<input id="filename" name="filename" type="text" class="hidden" value="'.$_SESSION['file'].$slide.'">';
					$html_open	.=	'<input name="image" type="file" id="upload" class="hidden" onchange="">';
					$html_close	 =	'</form></section>';
					echo($html_open);
					include($_SESSION['file'].$slide);
					echo($html_close);
				}
			} ?>
			</div>
			<div id="preview" class="sub-menu">
				<?php
					$i = 0;
					foreach($slides as $slide){
						//echo '<div class="thumbnail" data-thumbnail="'.$_SESSION['file'].$slide.'">';
						echo '<div id="box'.$i.'" class="thumbnail-container" ondrop="drop(event)" ondragover="allowDrop(event)">';
						echo '<div id="thumb'.basename($slide,'.html').'" class="thumbnail" draggable="true" ondragstart="drag(event)" data-thumbnail="'.basename($slide,'.html').'">';
						//echo (file_get_contents ($_SESSION['file'].$slide));
						include(($_SESSION['file'].$slide));
						echo '</div></div>';
						$i++;
					}
				?>
			</div>
		</div>
		<script src="lib/js/head.min.js"></script>
		<script src="js/reveal.js"></script>

		<script>
			// More info about config & dependencies:
			// - https://github.com/hakimel/reveal.js#configuration
			// - https://github.com/hakimel/reveal.js#dependencies
			Reveal.initialize({
				dependencies: [
					{ src: 'plugin/markdown/marked.js' },
					{ src: 'plugin/markdown/markdown.js' },
					{ src: 'plugin/notes/notes.js', async: true },
					{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
				]
			});
		</script>
	</body>
</html>
